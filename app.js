// set up SVG for D3
var width  = document.documentElement.clientWidth,
	height = document.documentElement.clientHeight;


var nodeSelected = false;
var zoom = d3.behavior.zoom().scaleExtent([0.5,2]).on("zoom", pan);
var svg = d3.select('body')
	.append('svg')
	.attr('oncontextmenu', 'return false;')
	.attr('width', width)
	.attr('height', height)
	.call(zoom);

// set up initial nodes and links
//  - nodes are known by 'id', not by index in array.
//  - links are always source < target; edge directions are set by 'left' and 'right'.
var nodes = [],
	links = [];
let nodes_by_id = {};

// init D3 force layout
var force = d3.layout.force()
	.nodes(nodes)
	.links(links)
	.size([width, height])
	.linkDistance(150)
	.charge(-500)
	.on('tick', tick);

// define arrow markers for graph links
svg.append('svg:defs').append('svg:marker')
	.attr('id', 'end-arrow')
	.attr('viewBox', '0 -5 10 10')
	.attr('refX', 6)
	.attr('markerWidth', 8)
	.attr('markerHeight', 8)
	.attr('orient', 'auto')
	.append('svg:path')
	.attr('d', 'M0,-5L10,0L0,5')
	.attr('fill', '#00BF00');

svg.append('svg:defs').append('svg:marker')
	.attr('id', 'start-arrow')
	.attr('viewBox', '0 -5 10 10')
	.attr('refX', 4)
	.attr('markerWidth', 8)
	.attr('markerHeight', 8)
	.attr('orient', 'auto')
	.append('svg:path')
	.attr('d', 'M10,-5L0,0L10,5')
	.attr('fill', '#00BF00');

// line displayed when dragging new nodes
var drag_line = svg.append('svg:path')
	.attr('class', 'link dragline hidden')
	.attr('d', 'M0,0L0,0');

var container = svg.append('svg:g')

// handles to link and node element groups
var path = container.append('svg:g').selectAll('path'),
	circle = container.append('svg:g').selectAll('g');

// update force layout (called automatically each iteration)
function tick() {
	// draw directed edges with proper padding from node centers
	path.attr('d', function(d) {
		var deltaX = d.target.x - d.source.x,
			deltaY = d.target.y - d.source.y,
			dist = Math.sqrt(deltaX * deltaX + deltaY * deltaY),
			normX = deltaX / dist,
			normY = deltaY / dist,
			sourcePadding = d.left ? 17 : 12,
			targetPadding = d.right ? 17 : 12,
			sourceX = d.source.x + (sourcePadding * normX),
			sourceY = d.source.y + (sourcePadding * normY),
			targetX = d.target.x - (targetPadding * normX),
			targetY = d.target.y - (targetPadding * normY);
		return 'M' + sourceX + ',' + sourceY + 'L' + targetX + ',' + targetY;
	});

	circle.attr('transform', function(d) {
		return 'translate(' + d.x + ',' + d.y + ')';
	});
}

var savedScale = null;
var savedTranslation = null;

function pan() {
	if(!nodeSelected){
		container.attr("transform", "translate(" + d3.event.translate + ")"
      	+ " scale(" + d3.event.scale + ")");
	}
}

// update graph (called when needed)
function restart() {
	// update size
	var width  = document.documentElement.clientWidth;
	var height = document.documentElement.clientHeight;

	svg
	  .attr("width", width)
	  .attr("height", height);

        force
	  .size([width, height])


	// path (link) group
	path = path.data(links);

	// update existing links
	path.style('marker-start', (d) => (d.left ? 'url(#start-arrow)' : ''))
		.style('marker-end', (d) => (d.right ? 'url(#end-arrow)' : ''));


	// add new links
	path.enter().append('svg:path')
		.attr('class', 'link')
		.style('marker-start', (d) => (d.left ? 'url(#start-arrow)' : ''))
		.style('marker-end', (d) => (d.right ? 'url(#end-arrow)' : ''));

	// remove old links
	path.exit().remove();


	// circle (node) group
	// NB: the function arg is crucial here! nodes are known by id, not by index!
	circle = circle.data(nodes, (d) => d.id);


	// add new nodes
	var g = circle.enter().append('svg:g').call(force.drag);

	g.append('svg:circle')
		.attr('class', 'node')
		.attr('r', 12)
		.style('fill', (d) => d3.rgb(0,0xBF,0))
		.style('stroke', (d) => d3.rgb(0,0xBF,0))

		.on('mousedown', function(d) {
			// enlarge target node
			d3.select(this).attr('transform', 'scale(1.1)');
			nodeSelected = true;
		  if (savedScale === null){
			  savedScale = zoom.scale()
			}
			if(savedTranslation === null){
				savedTranslation = zoom.translate()
			}
		})
		.on('mouseup', function(d) {
			// unenlarge target node
			d3.select(this).attr('transform', '');
		});

	// Show name
	g.append('svg:text')
		.attr('x', 0)
		.attr('y', 4)
		.attr('class', 'name')
		.text((d) => d.name);

	// Show ID
	g.append('svg:text')
		.attr('x', 0)
		.attr('y', 20)
		.attr('class', 'id')
		.text((d) => d.id);

	// remove old nodes
	circle.exit().remove();

	// set the graph in motion
	force.start();
}

document.addEventListener("mouseup", mouseup);

function mouseup() {
	// because :active only works in WebKit?
	nodeSelected = false;
	if (savedScale !== null){
	  zoom.scale(savedScale)
	  savedScale = null
	}
	if (savedTranslation !== null){
	  zoom.translate(savedTranslation)
	  savedTranslation = null
	}
}

function spliceLinksForNode(node) {
	var toSplice = links.filter(function(l) {
		return (l.source === node || l.target === node);
	});
	toSplice.map(function(l) {
		links.splice(links.indexOf(l), 1);
	});
}

function addKey(id, name, log) {
	if (log)
		logText(`<span class="log-name">${name} (${id})</span> just joined the keyserver – welcome!`);

	if (!(id in nodes_by_id)) {
		let node = {id: id, name: name};
		nodes.push(node);
		nodes_by_id[node.id] = node;
		restart();
		return node;
	} else {
		return nodes_by_id[id];
	}
}

function removeKey(id) {
    remove(nodes, nodes_by_id[id]);
    delete nodes_by_id[id];
    restart();
}

function remove(array, element) {
    const index = array.indexOf(element);
    if (index >= 0) {
      array.splice(index, 1);
    }
}

let log_inner = document.querySelectorAll(".log-inner")[0];
function logText(text) {
	let div = document.createElement("div");
	div.classList.add("log-entry");

	/////////////////////////////////////////////////////////////////
	// In this case it was really a Two Time Pad:                  //
	// Zrnwrbbiadp OLZ - Kkirf bcg yprszbp xjby! - Dtqvf & Vyed <3 //
	/////////////////////////////////////////////////////////////////
	div.innerHTML = text;

	div.addEventListener("animationend", (ev) => {log_inner.removeChild(div);});

	log_inner.appendChild(div);

}

function addSig(a, b, log) {
	if (log)
		logText(`<span class="log-name">${a.name} (${a.id})</span> signed <span class="log-name">${b.name} (${b.id})</span>'s key!`);

	let link = links.find(
		(x) => x.source == a && x.target == b
			|| x.target == a && x.source == b
	);

	if (link) {
		if (a == link.source)
			link.right = true;
		else
			link.left = true;
	} else {
		links.push({source: a, target: b, left: false, right: true});
	}
	restart();
}

function removeSig(a, b) {
	let link = links.find(
	   (x) => x.source == a && x.target == b
	      || x.target == a && x.source == b
	);
    if (link) {
        if (link.left && link.right) {
		        if (a == link.source)
			          link.right = false;
		        else
			          link.left = false;
        } else {
            remove(links, link);
        }
        restart();
    }
}

restart();

let socket = new ReconnectingWebSocket("wss://cybr.johnguant.com/ws");

socket.onmessage = (event) => {
	msg = JSON.parse(event.data);
    switch (msg.message_type) {
      case 'key':
		    addKey(msg.id, msg.name, !msg.is_connect);
        break;
      case 'sig':
		    addSig(nodes_by_id[msg.from], nodes_by_id[msg.to], !msg.is_connect);
        break;
      case 'rkey':
        removeKey(msg.id, msg.name);
        break;
      case 'rsig':
        removeSig(nodes_by_id[msg.from], nodes_by_id[msg.to]);
        break;
    }
}

window.addEventListener("resize", restart);
