#!/usr/bin/env ruby
require 'bundler/setup'
require 'eventmachine'
require 'em-websocket'
require 'set'
require 'json'

EM.run do
	@keys = Set.new
	@sigs = Set.new
	@channel = EM::Channel.new

	EM::WebSocket.run(:host => '127.0.0.1', :port => 30303) do |ws|
		sub_id = nil
		ws.onopen do |handshake|
			puts "new connection\n"

			@keys.each do |x|
				msg = x.dup
				msg[:message_type] = "key"
				msg[:is_connect] = true
				ws.send JSON.dump(msg)
			end

			@sigs.each do |x|
				msg = x.dup
				msg[:message_type] = "sig"
				msg[:is_connect] = true
				ws.send JSON.dump(msg)
			end

			sub_id = @channel.subscribe do |msg|
				ws.send JSON.dump(msg)
			end
		end

		ws.onmessage do |message|
			print "new message"
		end

		ws.onclose do
			@channel.unsubscribe(sub_id) unless sub_id.nil?
		end
	end

	EM.add_periodic_timer(1) do
		keys = Set.new
		known_ids = Set.new
		sigs = Set.new
		this_key = nil

		`curl -sS http://127.0.0.1/pks/lookup?op=x-get-bundle | gpg --list-packets`.lines do |l|
			case l
			when /^:public key packet/
				keys << this_key unless this_key.nil?
				this_key = {}
			when /^\tkeyid: .{8}(.{8})/
				unless this_key.include? :id
					this_key[:id] = "0x"+$1
					known_ids << "0x"+$1
				end
			when /^:signature packet: algo ., keyid .{8}(.{8})/
				sigs << {:from => "0x"+$1, :to => this_key[:id]} unless "0x"+$1 == this_key[:id]
			when /^:user ID packet: "(.*?)( \(.*\))?( <.*>)?"/
				this_key[:name] = $1
			end
		end
		keys << this_key unless this_key.nil?

		sigs.keep_if { |x| known_ids.include? x[:from] }

		(keys - @keys).each do |x|
			msg = x.dup
			msg[:message_type] = "key"
			msg[:is_connect] = false
			@channel.push msg
		end

		(sigs - @sigs).each do |x|
			msg = x.dup
			msg[:message_type] = "sig"
			msg[:is_connect] = false
			@channel.push msg
		end

		(@sigs - sigs).each do |x|
			msg = x.dup
			msg[:message_type] = "rsig"
			@channel.push msg
		end

		(@keys - keys).each do |x|
			msg = x.dup
			msg[:message_type] = "rkey"
			@channel.push msg
		end

		@keys = keys
		@sigs = sigs
	end
end
